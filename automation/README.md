# Using SONiC on AIR 
General notes on using the automation in this branch `sonic_air` to deploy EVPN Symmetric configs.  

Sections broken into the following categories:
1. General Notes
2. Automation Scope (presently)
3. Configuration Backup
4. Configuration Restore

## General Notes

The SONiC image on AIR currently uses statically defined interface naming conventions.  
In the future, the hard coded requirements will be changed to be more flexible with custom topologies.

**Static Interface Naming:**  
- Ethernet0
- Ethernet4
- Ethernet8
- Ethernet(N+4) ...

**Physical Interface to SONiC Interface Mapping**
- eth1 <---> Ethernet0
- eth2 <---> Ethernet4
- eth3 <---> Ethernet8
- eth(N+1) <---> Ethernet(N+4)

**SONiC Default Login:**  

| Credential | Value |
|----------|:--------|
| Username | admin |
| Password | sonic |


## Scope of this Branch (sonic_air)
The goal of this branch, under the `dc_configs_vxlan_evpnsymm` repository, is to supply a quick demo environment of EVPN Symm deployed on the CLDEMO2 architecture with the alteration of replacing `Spine03` and `Spine04` as SONiC nodes.

Configurations have been created and backed up within this branch to be easily deployed.  
To pair with that, simple automation has been created for the (re)deployment aspect.

Automation that renders configuration via templates has not been developed yet, that is to come later.  

_**Special Note**:_  
_Using this repository on AIR requires the ability to deploy a custom topology.  
To date, only NVIDIA employees have the necessary privileged access to do so.  
Right now this branch would not be useful to anyone outside of our organization, however, we are sharing this in order to provide reference configurations for what a mixed deployment of Cumulus & SONiC might look like._


## Configuration Backup Info
Configs have been backed up in the `./automation/playbooks/backup_files` directory for the following devices.  

* Border
* Spine
* Leaf
* Server

Other devices are currently not within scope of this exercise.  

Please review the directories for their file contents and to view the configurations for reference.


## Configuration Restore Info
How to perform a configuration deployment of EVPN Symm for the mixed topology containing Cumulus & SONiC ...  

First, note the the changes made within the `./automation/playbooks/` directory.  
The `restore_files.yml` playbook has been updated and the `restore_sonic.yml` playbook is new.  
In order to completely deploy/restore the backed up configurations in this repository both of those playbooks should be run.  

**Execute the following ansible commands to initiate the configuration restore process:**

1. Clone the repository to the `oob-mgmt-server`
```
$ git clone https://gitlab.com/cumulus-consulting/goldenturtle/dc_configs_vxlan_evpnsym.git
```
2. Move into the newly created project directory
```
$ cd dc_configs_vxlan_evpnsym
```
3. Checkout the `sonic_air` branch
```
$ git checkout sonic_air
```
4. Move into the `automation` directory
```
$ cd automation
```
5. Run the Ansible restore automation for the `Cumulus` nodes
```
$ ansible-playbook -i inventories/pod1/ playbooks/restore_files.yml
```
_Note: spines03 and spine04 will fail. This is expected._

6. Run the Ansible restore automation for the `SONiC` nodes
```
$ ansible-playbook -i inventories/pod1/ playbooks/restore_sonic.yml
```
7. Log into various nodes and confirm BGP sessions are up for IPv4 AF and EVPN
_Note: EVPN neighbors only exist between leaf nodes. Spines do not have EVPN configured._