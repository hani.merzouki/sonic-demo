# SONIC Demo

This repository is meant to work in conjunction with the `SONIC` topology on the NVIDIA Air Infrastructure Simulation Platform.

This architecture is based on the cldemo2 topology: https://gitlab.com/cumulus-consulting/goldenturtle/cldemo2

Instead of using Cumulus Linux, it uses SONIC as the NOS for all the nodes in the topology.

This repository holds all the configurations for a deployment using the following technologies:

* BGP numbered architecture
* Route peering to the servers
* Layer 3 connectivity only

## Quick Start

1. Request a `SONIC Demo` topology on https://air.nvidia.com. Please contact your sales representatives if you're having issue accomplishing this, or reach out on the Cumulus public slack: cumulusnetworks.slack.com

2. Log into the `OOB MGMT Server`

3. Clone this repo
```
git clone https://gitlab.com/cumulus-consulting/goldenturtle/sonic-demo.git && cd sonic-demo
```

4. Move into the `automation` directory
```
$ cd automation
```
5. Run the Ansible restore automation for the `SONiC` nodes
```
$ ansible-playbook -i inventories/pod1/ playbooks/restore_sonic.yml
```

6. Run the Ansible restore automation for the `server` nodes
```
$ ansible-playbook -i inventories/pod1/ playbooks/restore_files.yml
```
7. Log into various nodes and confirm BGP sessions are up for IPv4 AF

<!-- AIR:page -->

## SONIC login

To log into the SONIC switches, use the following credentials:

| Credential | Value |
|----------|:--------|
| Username | admin |
| Password | YourPaSsWoRd |

This is the default username and password for SONIC images, as set by the upstream repository.

## SONIC Verification

Verify the LLDP neighbors for spine03:

```
admin@leaf01:~$ show lldp table
Capability codes: (R) Router, (B) Bridge, (O) Other
LocalPort    RemoteDevice     RemotePortID       Capability    RemotePortDescr
-----------  ---------------  -----------------  ------------  -----------------
Ethernet0    server01         44:38:39:00:00:32  R             eth1
Ethernet4    server02         44:38:39:00:00:34  O             eth1
Ethernet8    server03         44:38:39:00:00:36  O             eth1
Ethernet12   leaf02           fortyGigE0/12      BR            Ethernet12
Ethernet16   leaf02           fortyGigE0/16      BR            Ethernet16
Ethernet20   spine03          fortyGigE0/0       BR            Ethernet0
Ethernet24   spine03          fortyGigE0/0       BR            Ethernet0
Ethernet28   spine03          fortyGigE0/0       BR            Ethernet0
Ethernet32   spine04          fortyGigE0/0       BR            Ethernet0
eth0         oob-mgmt-switch  swp10              BR            swp10
--------------------------------------------------
Total entries displayed:  10
```

Verify IP addresses on interface:

```
admin@leaf01:~$ show ip interface
Interface    Master    IPv4 address/mask    Admin/Oper    BGP Neighbor    Neighbor IP
-----------  --------  -------------------  ------------  --------------  -------------
Ethernet20             172.0.1.1/31         up/up         spine01         172.0.1.0
Ethernet24             172.0.2.1/31         up/up         spine02         172.0.2.0
Ethernet28             172.0.3.1/31         up/up         spine03         172.0.3.0
Ethernet32             172.0.4.1/31         up/up         spine04         172.0.4.0
Loopback0              10.255.255.1/32      up/up         N/A             N/A
Vlan101                10.0.101.1/24        up/up         N/A             N/A
docker0                240.127.1.1/24       up/down       N/A             N/A
eth0                   192.168.200.11/24    up/up         N/A             N/A
lo                     127.0.0.1/8          up/up         N/A             N/A
```

Verify that routing peers are established on spine03.

```
admin@leaf01:~$ show ip bgp summary

IPv4 Unicast Summary:
BGP router identifier 10.255.255.1, local AS number 65101 vrf-id 0
BGP table version 31
RIB entries 29, using 5336 bytes of memory
Peers 7, using 146440 KiB of memory
Peer groups 5, using 320 bytes of memory


Neighbhor       V     AS    MsgRcvd    MsgSent    TblVer    InQ    OutQ  Up/Down      State/PfxRcd  NeighborName
------------  ---  -----  ---------  ---------  --------  -----  ------  ---------  --------------  --------------
10.0.101.101    4  65201        308        307         0      0       0  05:04:14                1  SERVER
10.0.101.102    4  65201        308        307         0      0       0  05:04:14                1  SERVER
10.0.101.103    4  65201        308        307         0      0       0  05:04:14                1  SERVER
172.0.1.0       4  65199       8380       8388         0      0       0  06:58:09                5  spine01
172.0.2.0       4  65199       8378       8383         0      0       0  06:58:03                5  spine02
172.0.3.0       4  65199       8379       8385         0      0       0  06:58:05                5  spine03
172.0.4.0       4  65199       8380       8387         0      0       0  06:58:07                5  spine04

Total number of neighbors 7
```

Note: this output may render incorrectly due to the bug identified here: https://github.com/Azure/sonic-utilities/pull/1194

<!-- AIR:page -->

## Demo architecture

### IPAM

Servers are configured for access vlan. The servers use a route for 10.0.0.0/8 pointing to the firewall (10.1.<VLAN#>.1).

```
server01 - lo   - 192.168.255.1/32
server01 - eth1 - 10.0.101.101/24
server01 - eth2 - 10.0.102.101/24

server02 - lo   - 192.168.255.2/32
server02 - eth1 - 10.0.101.102/24
server02 - eth2 - 10.0.102.102/24

server03 - lo   - 192.168.255.3/32
server03 - eth1 - 10.0.101.103/24
server03 - eth2 - 10.0.102.103/24

server04 - lo   - 192.168.255.4/32
server04 - eth1 - 10.0.103.104/24
server04 - eth2 - 10.0.104.104/24

server05 - lo   - 192.168.255.5/32
server05 - eth1 - 10.0.103.105/24
server05 - eth2 - 10.0.104.105/24

server06 - lo   - 192.168.255.6/32
server06 - eth1 - 10.0.103.106/24
server06 - eth2 - 10.0.104.106/24

leaf01   - vlan 101 - 10.0.101.1/24
leaf02   - vlan 102 - 10.0.102.1/24
leaf03   - vlan 103 - 10.1.103.1/24
leaf04   - vlan 104 - 10.1.104.1/24
border01 - vlan 105 - 10.1.105.1/24
border02 - vlan 106 - 10.1.106.1/24
```

### Features

This automation repo is used to demonstrate the following features:
 * BGP numbered fabric connectivity
 * Dynamic BGP peering to servers
 * Pure L3 routed infrastructure

## Automation

### Ansible

Prerequisites:
- Cumulus Linux and SONIC Reference Topology has already been started and is running, this will be done within the NVIDIA Air Infrastructure Simulation platform
- From a shell session on the oob-mgmt-server inside of the simulation

1) Clone the repo
```
git clone https://gitlab.com/cumulus-consulting/goldenturtle/sonic-demo.git && cd sonic-demo
```

2) Test ansible
```
ansible pod1 -i inventories/pod1 -m ping
```

3) Run the ansible playbook to deploy the demo to the fabric
```
ansible-playbook playbooks/restore_files.yml -i inventories/pod1
ansible-playbook playbooks/restore_sonic.yml -i inventories/pod1
```

### Playbook Structure

The playbooks have the following important structure:
* Variables and inventories are stored in the same directory `automation/inventories/pod1/group_vars/`
* Backup configurations are stored in `automation/playbooks/configs/`
<!-- AIR:page -->
